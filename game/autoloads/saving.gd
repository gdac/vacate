extends Node
# Handles saving and loading progress of game.


const QUICK_FILE = "quick"


func has_quick():
	var file: File = File.new()
	return file.file_exists(_construct_path(QUICK_FILE))


func load_quick() -> Dictionary:
	return _load(QUICK_FILE)


func save_quick(data: Dictionary):
	_save(QUICK_FILE, data)


func _construct_path(file_name: String) -> String:
	return "user://%s.save" % file_name


func _load(file_name: String):
	var file: File = File.new()
	var path := _construct_path(file_name)

	if not file.file_exists(path):
		return null

	file.open(path, File.READ)
	var data: Dictionary = parse_json(file.get_as_text())
	file.close()

	return data


func _save(file_name: String, data: Dictionary):
	var file: File = File.new()
	var err := file.open(_construct_path(file_name), File.WRITE)

	if err == OK:
		file.store_line(to_json(data))
	else:
		print("Failed to save")

	file.close()
