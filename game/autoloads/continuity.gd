extends Node
# Ensures that things stay continuous across room loads.


var _globals := {}
var _room_id := ""
var _rooms = {}


func complete_puzzle(key: String):
	get_continuity().complete_puzzle.append(key)


func get_all():
	return {
		"id": _room_id,
		"globals": _globals,
		"rooms": _rooms,
	}


func get_continuity():
	return _rooms[_room_id]


func get_custom(key: String):
	var custom = get_continuity().custom
	return custom[key] if custom.has(key) else null


func get_global(key: String):
	return _globals[key] if _globals.has(key) else null


func get_muffled_vent_count() -> int:
	var count := 0

	for i in _rooms:
		count += int(_rooms[i].custom.has("Vent"))

	return count


# Report how many puzzles have been completed.
func get_progress() -> int:
	var progress := 0

	for i in _rooms:
		progress += _rooms[i].complete_puzzle.size()

	return progress


func remove_interactable(item: String):
	get_continuity().remove_interactable.append(item)


func set_all(data):
	_room_id = data.id
	_rooms = data.rooms


func set_custom(key: String, data):
	get_continuity().custom[key] = data


func set_global(key: String, data):
	_globals[key] = data


func set_room(room_id: String):
	_room_id = room_id

	if not _rooms.has(_room_id):
		_rooms[_room_id] = {
			"custom": {},
			"complete_puzzle": [],
			"remove_interactable": [],
			"unlock_door": [],
		}


func unlock_door(door_id: String):
	get_continuity().unlock_door.append(door_id)
