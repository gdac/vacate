extends Node
# Holds all the game items currently held by player.


signal modified

var _combinations = [
	{
		"inputs": ["Cog", "Spoke"],
		"result": {
			"text": "Cog with Spoke",
			"texture_path": "res://assets/images/items/cog_with_spoke.png",
		},
	},
	{
		"inputs": ["Stack of Flyers", "Glue"],
		"result": {
			"text": "Glued Stack of Flyers",
			"texture_path": "res://assets/images/items/glued_flyer_stack.png",
		},
	},
]
var _items := []


func add(text: String, texture_path: String):
	_items.push_back({
		"text": text,
		"texture_path": texture_path
	})
	emit_signal("modified")


func combine(item1: String, item2: String) -> bool:
	var indexes := [_get_one_index(item1), _get_one_index(item2)]
	indexes.sort()

	if indexes[0] == -1 or indexes[1] == -1:
		return false

	var combo := get_combination(item1, item2)

	if not combo:
		return false

	_items.remove(indexes[1])
	_items.remove(indexes[0])
	add(combo.result.text, combo.result.texture_path)
	return true


func delete(item: String):
	var index := -1
	for i in range(0, _items.size()):
		if _items[i].text == item:
			index = i
			continue

	if index == -1:
		printerr("Cannot delete item.")
		return

	_items.remove(index)
	_on_modified()


func get_all() -> Array:
	return _items


func get_combination(item1: String, item2: String) -> Object:
	for combo in _combinations:
		if combo.inputs.has(item1) and combo.inputs.has(item2):
			return combo

	return null


func get_one(index: int):
	return _items[index]


func remove(index: int):
	_items.remove(index)
	call_deferred("_on_modified")


func set_all(data: Array):
	_items = data
	call_deferred("_on_modified")


func set_one(index: int, text: String, texture_path: String):
	_items[index] = {
		"text": text,
		"texture_path": texture_path
	}
	emit_signal("modified")


func _get_one_index(item: String) -> int:
	for i in range(0, _items.size()):
		if _items[i].text == item:
			return i

	return -1


func _on_modified():
	emit_signal("modified")
