class_name InventoryBar
extends Control
# Display of the current inventory, also allowing for using items.


signal item_selected(text)

var selected_item: InventoryItem = null

onready var bar: Control = $Bar
onready var item_title: ItemTitle = $Bar/ItemTitle
onready var items: Array = $Bar/InventoryItems.get_children()
onready var toggle: Button = $Bar/Toggle
onready var toggle_tween: Tween = $Bar/Toggle/Tween


func _ready():
	Inventory.connect("modified", self, "update_data")
	update_data()

	for item in items:
		item.connect("pressed", self, "_on_InventoryItem_pressed", [item])
		item.connect("title_set", self, "set_item_title")


func cancel_selection():
	_select_item(null)
	set_item_title("")


func set_item_title(text: String):
	if selected_item:
		if text == selected_item.title:
			item_title.text = "Don't use %s" % selected_item.title
			return

		if text != "":
			item_title.text = "Use %s on %s" % [selected_item.title, text]
			return

		item_title.text = "Use %s on" % selected_item.title
		return

	item_title.text = text


func update_data():
	var data := Inventory.get_all()

	for i in range(0, items.size()):
		var item: InventoryItem = items[i]

		if i < data.size():
			item.setup(data[i].text, data[i].texture_path)
		else:
			item.clear()


func _on_InventoryItem_pressed(item: InventoryItem):
	# Cancel if selecting same item or a blank item
	if item.title == "" or selected_item == item:
		_select_item(null)
		set_item_title("")
		return

	# Select item if nothing is selected
	if not selected_item:
		_select_item(item)
		set_item_title(selected_item.title)
		return

	# Combine item if possible
	Inventory.combine(item.title, selected_item.title)
	_select_item(null)
	return


func _on_Toggle_toggled(on: bool):
	toggle_tween.stop_all()
	var goal: float

	if on:
		goal = -240
		toggle.text = "Hide Items"
	else:
		goal = 0
		toggle.text = "Show Items"

	toggle_tween.interpolate_property(bar, "rect_position:y", bar.rect_position.y, goal, .25)
	toggle_tween.start()


func _select_item(item):
	selected_item = item
	emit_signal("item_selected", item.title if item else "")
