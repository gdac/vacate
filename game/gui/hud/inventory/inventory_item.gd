class_name InventoryItem
extends ToolButton
# Item in the inventory list.


signal title_set(text)

var title := ""

onready var pivot: Control = $Pivot
onready var texture_rect: TextureRect = $Pivot/TextureRect
onready var tween: Tween = $Tween


func _ready():
	texture_rect.modulate.a = .5


func clear():
	title = ""
	texture_rect.texture = null


func setup(item_text: String, texture_path: String):
	title = item_text
	texture_rect.texture = load(texture_path)

	# Item bounce on add to inventory - maybe try and make this work sometime
#	_set_pivot_phase(0.0)
#	tween.interpolate_method(self, "_set_pivot_phase", 0.0, 1.0, .5)
#	tween.start()


func _on_InventoryItem_mouse_entered():
	texture_rect.modulate.a = 1
	emit_signal("title_set", title)


func _on_InventoryItem_mouse_exited():
	texture_rect.modulate.a = .5
	emit_signal("title_set", "")


func _set_pivot_phase(phase: float):
	pivot.rect_position.y = pivot.rect_size.y * (cos((1 - phase) * PI * 1.5) - 1) * .375
	pivot.rect_scale = Vector2(phase, phase)
