class_name HUD
extends CanvasLayer
# Heads up display shown during gameplay.


signal curtain_faded
signal fullscreen_toggled
signal inventory_item_selected(text)
signal next_passage_requested
signal quick_load_requested
signal saved
signal unpaused

const ROOM_FADE_SPEED := .666

onready var curtain: Curtain = $Curtain
onready var dialogue: Dialogue = $Dialogue
onready var inventory: InventoryBar = $InventoryBar
onready var item_pickup_display: ItemPickupDisplay = $ItemPickupDisplay
onready var pause_menu: PauseMenu = $PauseMenu


func _ready():
	dialogue.hide()
	item_pickup_display.hide()
	pause_menu.hide()

	on_room_enter()


func cancel_inventory_selection():
	inventory.cancel_selection()


func dialogue_playback_begin():
	inventory.hide()
	dialogue.show()


func dialogue_playback_end():
	inventory.show()
	dialogue.hide()


func dialogue_triggered(speaker: String, text: String):
	dialogue.triggered(speaker, text)


func display_puzzle(puzzle: Puzzle):
	curtain.fade_to(.5, .5)
	yield(curtain, "faded")

	add_child(puzzle)
	puzzle.owner = self
	yield(puzzle, "closed")

	puzzle.queue_free()
	curtain.fade_to(0.0, .5)
	yield(curtain, "faded")
	emit_signal("unpaused")


func on_room_enter():
	curtain.fade_to(0.0, ROOM_FADE_SPEED)
	yield(curtain, "faded")
	emit_signal("unpaused")


func on_room_exit():
	curtain.fade_to(1.0, ROOM_FADE_SPEED)


func pause():
	curtain.fade_to(.25, .5)
	yield(curtain, "faded")
	pause_menu.show()


func set_hovered_item(text: String):
	inventory.set_item_title(text)


func show_item_pickup(item: String, texture_path: String):
	curtain.fade_to(.5, .5)
	item_pickup_display.display(item, texture_path)
	yield(item_pickup_display, "begin_fading_out")
	curtain.fade_to(0.0, .5)
	yield(curtain, "faded")
	emit_signal("unpaused")


func _on_Dialogue_next_passage_requested():
	emit_signal("next_passage_requested")


func _on_Curtain_faded():
	emit_signal("curtain_faded")


func _on_InventoryBar_item_selected(text):
	emit_signal("inventory_item_selected", text)


func _on_PauseMenu_fullscreen_toggled():
	emit_signal("fullscreen_toggled")


func _on_PauseMenu_saved():
	emit_signal("saved")


func _on_PauseMenu_unpaused(should_load: bool):
	pause_menu.hide()
	curtain.fade_to(0.0, .5)
	yield(curtain, "faded")

	if should_load:
		emit_signal("quick_load_requested")

	emit_signal("unpaused")
