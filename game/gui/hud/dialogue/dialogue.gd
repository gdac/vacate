class_name Dialogue
extends Control
# Class which visually shows the dialogue playing out.


signal next_passage_requested

const SCROLL_SPEED := 66.6

onready var body_label: Label = $Body/Label
onready var header_label: Label = $Header/Label


func _process(delta: float):
	var length = body_label.text.length()

	if length == 0:
		body_label.percent_visible = 1
		return

	body_label.percent_visible += delta * SCROLL_SPEED / length


func show():
	body_label.text = ""
	header_label.text = ""
	.show()


func skip():
	if body_label.percent_visible < 1:
		body_label.percent_visible = 1
		return

	emit_signal("next_passage_requested")


func triggered(speaker: String, text: String):
	body_label.percent_visible = 0
	body_label.text = text
	header_label.text = speaker
