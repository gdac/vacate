class_name PauseMenu
extends VBoxContainer
# Menu to be shown when game is paused.


signal fullscreen_toggled
signal saved
signal unpaused(should_load)


func _ready():
	$Quickload.visible = Saving.has_quick()
	$QuitGame.visible = OS.has_feature("pc")


func _on_Quickload_pressed():
	emit_signal("unpaused", true)


func _on_Quicksave_pressed():
	emit_signal("unpaused", false)
	emit_signal("saved")


func _on_QuitGame_pressed():
	get_tree().quit()


func _on_ResumeGame_pressed():
	emit_signal("unpaused", false)


func _on_ToggleFullscreen_pressed():
	emit_signal("fullscreen_toggled")
