extends CheckBox


func _on_PushButton_toggled(button_pressed: bool):
	if button_pressed:
		$Down.play()
		return

	$Up.play()
