class_name SwitchesPuzzle
extends "res://gui/puzzles/puzzle.gd"
# Puzzle with many switches that need to be pressed in right order


const DEFAULT_VALUES := [-1, -1, -1]
const MODIFIERS := [
	[-1, 1, 1],
	[1, -1, 0],
	[1, -1, 1],
	[0, -1, 1],
	[1, 1, -1],
]

var _values := []

onready var gauges := $VBoxContainer/Gauges.get_children()
onready var switches := $VBoxContainer/Switches.get_children()


func _ready():
	for switch in switches:
		switch.connect("pressed", self, "_update")

	_update()


func _update():
	_values = DEFAULT_VALUES.duplicate()

	# Calculate current value
	for i in range(0, switches.size()):
		if switches[i].pressed:
			_values[0] += MODIFIERS[i][0]
			_values[1] += MODIFIERS[i][1]
			_values[2] += MODIFIERS[i][2]

	# Show gauge values
	for i in range(0, gauges.size()):
		gauges[i].value = _values[i]

	# Determine if puzzle is complete
	if _values == [0, 0, 0]:
		_on_completed()
