class_name PressureGauge
extends Control
# Gauge to show pressure between -3 and 3.


var value := -3

onready var arm: TextureRect = $Background/Arm


func _process(delta: float):
	var r = randf() - .5
	var rot = lerp(arm.rect_rotation, (value + 3) * 15 + r * 20, 5 * delta)
	arm.rect_rotation = clamp(rot, 0, 90)
