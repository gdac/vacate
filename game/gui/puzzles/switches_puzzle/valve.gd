class_name Valve
extends CheckBox
# A spinning valve which can be clicked like a check box.


const SPEED := 2.0

var _phase := 0.0

onready var texture_rect: TextureRect = $TextureRect


func _process(delta: float):
	var i: float = 1.0 if pressed else -1.0
	_phase = clamp(_phase + i * delta * SPEED, 0, 1)
	rect_rotation = ease(_phase, -1.8) * -222


func _on_Valve_pressed():
	$Turn.play()
