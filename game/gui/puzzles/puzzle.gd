class_name Puzzle
extends Control
# A puzzle to be solved by the player.


signal closed
signal completed


func _on_CloseButton_pressed():
	emit_signal("closed")


func _on_completed():
	emit_signal("completed")
	emit_signal("closed")
