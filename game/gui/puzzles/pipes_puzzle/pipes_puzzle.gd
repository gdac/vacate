class_name PipesPuzzle
extends "res://gui/puzzles/puzzle.gd"


var _selected_pipe: Pipe

onready var _crawler_left: PipeCrawler = $PipeCrawlerLeft
onready var _crawler_right: PipeCrawler = $PipeCrawlerRight
onready var _grid: GridContainer = $Grid
onready var _pipes := _get_all_pipes()
onready var _sfx_pick_up: AudioStreamPlayer = $SFX/PickUp
onready var _sfx_put_down: AudioStreamPlayer = $SFX/PutDown


func _get_all_pipes() -> Array:
	var pipes := []

	for cell in $Grid.get_children():
		if cell is Pipe:
			cell.connect("pressed", self, "_on_Pipe_pressed", [cell])
			pipes.append(cell)

	return pipes


func _on_Pipe_pressed(pipe: Pipe):
	if not _selected_pipe:
		_selected_pipe = pipe
		_selected_pipe.selected = true
		_sfx_pick_up.play()
		return

	_sfx_put_down.play()

	if _selected_pipe != pipe:
		var index := _selected_pipe.get_index()
		_grid.move_child(_selected_pipe, pipe.get_index())
		_grid.move_child(pipe, index)
		if _crawler_left.evaluate() and _crawler_right.evaluate():
			_on_completed()

	_selected_pipe.selected = false
	_selected_pipe = null
