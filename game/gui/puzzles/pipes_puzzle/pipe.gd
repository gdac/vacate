class_name Pipe
extends ToolButton
# Pipe to be used in pipe puzzle.


var selected := false

onready var _highlight: TextureButton = $Highlight


func _process(_delta: float):
	_highlight.visible = selected


func get_letters() -> String:
	var regex: RegEx = RegEx.new()
	regex.compile("Pipe(\\w+)[0-9]*")
	return regex.search(name).strings[1]
