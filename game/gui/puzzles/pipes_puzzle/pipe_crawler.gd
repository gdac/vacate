class_name PipeCrawler
extends Node
# Tries to find a path along pipes from start index to finish index.


enum Target {
	GOAL,
	IN_HISTORY,
	NOT_FOUND,
	NOT_TRAVERSABLE,
	TEXTURE_RECT
	TRAVERSABLE,
}

const MATCHES = {
	"L": "R",
	"R": "L",
	"U": "D",
	"D": "U",
}

const OFFSETS = {
	"L": -1,
	"R": 1,
	"U": -7,
	"D": 7,
}

export(NodePath) var grid_path
export(int) var start_node_index: int
export(NodePath) var end_node_path

var _found_goal := false
var _found_start := false
var _history := []
var _index := start_node_index

onready var _end_node_index := get_node(end_node_path).get_index()
onready var _grid: GridContainer = get_node(grid_path)
onready var _grid_node_count: int = _grid.get_children().size()


func evaluate() -> bool:
	_found_goal = false
	_found_start = false
	_history = []
	return _check_path_from(start_node_index) and _found_goal and _found_start


# Returns whether or not an index exists on the grid.
func _check_index_is_valid(index: int) -> bool:
	return index >= 0 and index < _grid_node_count


func _check_path_from(index: int) -> bool:
	var pipe: Pipe = _grid.get_child(index)
	_history.append(pipe)

	for letter in pipe.get_letters():
		if int(letter) != 0:
			continue

		var next_index: int = index + OFFSETS[letter]

		match _check_traversal(letter, next_index):
			Target.GOAL:
				_found_goal = true

			Target.NOT_FOUND, Target.NOT_TRAVERSABLE:
				return false

			Target.TEXTURE_RECT:
				_found_start = true

			Target.TRAVERSABLE:
				if not _check_path_from(next_index):
					return false

	return true


func _check_traversal(direction: String, end_index: int) -> int:
	if end_index == _end_node_index:
		return Target.GOAL

	if not _check_index_is_valid(end_index):
		return Target.NOT_FOUND

	var end_node = _grid.get_child(end_index)

	if end_node is TextureRect:
		return Target.TEXTURE_RECT

	if _history.has(end_node):
		if end_node is Pipe and end_node.get_letters().find(MATCHES[direction]) == -1:
			return Target.NOT_TRAVERSABLE

		return Target.IN_HISTORY

	if end_node is Pipe and end_node.get_letters().find(MATCHES[direction]) >= 0:
		return Target.TRAVERSABLE

	return Target.NOT_TRAVERSABLE
