class_name Paper
extends TextureButton
# Sheet of paper from paper puzzle.


signal moved

var _offset: Vector2
var _selected := false


func _process(_delta):
	if _selected:
		rect_position += get_local_mouse_position() - _offset
		rect_position.x = clamp(rect_position.x, 150, 1770 - rect_size.x)
		rect_position.y = clamp(rect_position.y, 150, 930 - rect_size.y)


func _on_Paper_button_down():
	_selected = true
	_offset = get_local_mouse_position()


func _on_Paper_button_up():
	_selected = false
	emit_signal("moved")
