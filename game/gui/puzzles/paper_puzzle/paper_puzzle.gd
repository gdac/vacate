class_name PaperPuzzle
extends "res://gui/puzzles/puzzle.gd"
# Puzzle where you have to arrange papers into pattern.


onready var papers: Control = $Papers


func _ready():
	var positions_set := _load_positioning()

	for item in papers.get_children():
		item.connect("moved", self, "_save_positioning")

		if not positions_set:
			item.rect_position = Vector2(
					960 + (600 - randf() * 1200) - item.rect_size.x / 2,
					540 + (300 - randf() * 600) - item.rect_size.y / 2
			)

	if not positions_set:
		_save_positioning()


func _load_positioning() -> bool:
	var data = Continuity.get_custom("paper_puzzle")

	if not data:
		return false

	for key in data:
		var pos = data[key]
		var node = papers.get_node(key)
		node.rect_position.x = pos.x
		node.rect_position.y = pos.y

	return true


func _save_positioning():
	var tmp = {}

	for item in papers.get_children():
		tmp[item.name] = {
			"x": item.rect_position.x,
			"y": item.rect_position.y,
		}

	Continuity.set_custom("paper_puzzle", tmp)
