class_name Lever
extends VSlider
# Lever that can be slid.


var _selected := false


func _ready():
	_update_levers(value)
	$Hover.visible = false


func _process(_delta):
	if _selected and Input.is_action_just_released("click"):
		$SFX/Release.play()
		_selected = false
		return

	if not _selected and $Hover.visible and Input.is_action_just_pressed("click"):
		$SFX/Hold.play()
		_selected = true
		return


func _on_Lever_mouse_entered():
	$Normal.visible = false
	$Hover.visible = true


func _on_Lever_mouse_exited():
	$Normal.visible = true
	$Hover.visible = false


func _on_Lever_value_changed(value):
	$SFX/Move.play()
	_update_levers(value)


func _update_levers(value):
	for i in range(0, 5):
		$Normal.get_child(i).visible = value == i
		$Hover.get_child(i).visible = value == i
