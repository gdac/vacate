class_name LeversPuzzle
extends "res://gui/puzzles/puzzle.gd"
# A puzzle in which requires levers to be placed in the right positions.


var _solution := [3, 1, 2, 2, 4]
var _values := []


func _ready():
	for lever in $HBoxContainer/Levers.get_children():
		lever.connect("value_changed", self, "_on_Lever_value_changed", [_values.size()])
		_values.append(lever.value)


func _on_Button_pressed():
	if _values == _solution:
		_on_completed()


func _on_Lever_value_changed(value: int, index: int):
	_values[index] = value
