class_name Splash
extends Control
# A full screen timed fade-in-fade-out display.


signal completed

export(float) var time := 2.0


func _ready():
	hide()
