extends "res://states/state.gd"
# Shows introduction splashes.


onready var curtain: Curtain = $Curtain
onready var label: Label = $Label
onready var sprite: AnimatedSprite = $AnimatedSprite
onready var tween: Tween = $Tween


func _ready():
	label.modulate.a = 0
	curtain.fade_to(0, .666)
	yield(curtain, "faded")
	sprite.playing = true


func _on_AnimatedSprite_animation_finished():
	tween.interpolate_method(self, "_update", 0.0, 1.0, 1.0)
	tween.start()
	yield(tween, "tween_completed")
	yield(get_tree().create_timer(1), "timeout")
	curtain.fade_to(1, .666)
	yield(curtain, "faded")
	emit_signal("change", "res://states/main_menu/main_menu_state.tscn")


func _update(value: float):
	var s = 2.666 - .666 * ease(value, -4)
	sprite.scale.x = s
	sprite.scale.y = s

	label.modulate.a = max(0, value * 2 - 1)
