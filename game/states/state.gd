tool
class_name State
extends Node
# An encompassing slice of game functionality.


signal change(state_path, _load_game)
signal fullscreen_toggled


func _process(delta):
	if Input.is_action_just_pressed("toggle_fullscreen"):
		emit_signal("fullscreen_toggled")
