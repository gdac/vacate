class_name GameplayMusicManager
extends Node
# Manages the music and screams for the general gameplay.


const SCREAM_TIME := 1.66
const SCREAM_TIME_VARIANCE := .34

var _screams := []

onready var scream_timer: Timer = $ScreamTimer


func _ready():
	_generate_screams()


func begin_screams():
	var puzzles_completed := Continuity.get_progress()
	for bgm in $BGM.get_children():
		bgm.volume_db = linear2db(int(bgm.get_index() <= puzzles_completed))
	if $ScreamTimer.is_stopped():
		$ScreamTimer.start()


func end_screams():
	for bgm in $BGM.get_children():
		bgm.volume_db = linear2db(0.0)
	$ScreamTimer.stop()


func _generate_screams():
	_screams = $Screams.get_children()
	_screams.shuffle()


func _on_ScreamTimer_timeout():
	_play_next_scream()
	scream_timer.wait_time = SCREAM_TIME + SCREAM_TIME_VARIANCE * randf()
	scream_timer.start()


func _play_next_scream():
	var scream: AudioStreamPlayer = _screams.pop_back()
	scream.play()

	if _screams.size() == 0:
		_generate_screams()


