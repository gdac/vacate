extends State
# State which handles general gameplay.


signal game_saved

var current_room: Room setget ,_get_current_room
var save_data = {}

onready var hud: HUD = $HUD
onready var music_manager: GameplayMusicManager = $GameplayMusicManager
onready var yarn_player: YarnPlayer = $YarnPlayer


func _ready():
	var room := _get_current_room()

	if room:
		_setup_room(room)


func _process(delta):
	if _get_current_room().is_paused:
		return

	if Input.is_action_just_pressed("quicksave"):
		save_game()

	if Input.is_action_just_pressed("quickload"):
		load_game()


func load_game():
	var data = Saving.load_quick()

	if not data:
		print("Failed to load")
		return

	Continuity.set_all(data.continuity)
	Inventory.set_all(data.inventory)
	_YarnStore.set_all(data.yarn)
	_load_room(data.room_path)
	_get_current_room().player.position = Vector2(data.player.x, data.player.y)


func save_game():
	var room := _get_current_room()
	Saving.save_quick({
		"room_path": room.filename,
		"continuity": Continuity.get_all(),
		"inventory": Inventory.get_all(),
		"player": {
			"x": room.player.position.x,
			"y": room.player.position.y,
		},
		"yarn": _YarnStore.get_all(),
	})


func _add_inventory_item(text: String, texture_path: String):
	Inventory.add(text, texture_path)
	hud.show_item_pickup(text, texture_path)


func _change_state(state: String):
	emit_signal("change", state)


# Inform the user if no child of type Room is found.
func _get_configuration_warning() -> String:
	if not _get_current_room():
		return "No child of type \"Room\" found."

	return ""


# Search for the current room and return if found.
func _get_current_room() -> Room:
	for child in get_children():
		if child is Room:
			return child

	return null


func _load_room(target_scene: String) -> Room:
	var room := _get_current_room()
	if room:
		remove_child(room)
		room.queue_free()

	room = load(target_scene).instance()
	add_child(room)
	room.set_owner(self)
	_setup_room(room)
	hud.on_room_enter()
	return room


func _on_HUD_fullscreen_toggled():
	emit_signal("fullscreen_toggled")


func _on_HUD_inventory_item_selected(text: String):
	_get_current_room().inventory_bar_selection = text


func _on_HUD_next_passage_requested():
	yarn_player.do_next()


func _on_HUD_quick_load_requested():
	load_game()


func _on_HUD_saved():
	save_game()


func _on_Room_door_entered(target_door: String, target_scene: String):
	var room := _get_current_room()
	hud.on_room_exit()
	yield(hud, "curtain_faded")
	remove_child(room)
	room.queue_free()
	room = null

	room = _load_room(target_scene)
	room.jump_player_to_door(target_door)


func _on_Room_item_consumed(item: String):
	Inventory.delete(item)
	hud.cancel_inventory_selection()


func _on_YarnPlayer_dialogue_triggered(speaker, text):
	hud.dialogue_triggered(speaker, text)


func _on_YarnPlayer_playback_began():
	hud.dialogue_playback_begin()


func _on_YarnPlayer_playback_ended():
	hud.dialogue_playback_end()
	_get_current_room().unpause()


# Set up room
func _setup_room(room: Room):
	hud.inventory.visible = room.name != "CellWalkCutscene" and room.name != "Credits" and room.name != "CatCutscene" and room.name != "DogCutscene"

	match room.name:
		"CellRoom", "CellWalkCutscene", "Credits", "CatCutscene", "DogCutscene":
			music_manager.end_screams()

		_:
			music_manager.begin_screams()

	hud.connect("unpaused", room, "unpause")
	room.connect("cancel_inventory_selection", hud, "cancel_inventory_selection")
	room.connect("change_state", self, "_change_state")
	room.connect("door_entered", self, "_on_Room_door_entered")
	room.connect("dialogue_started", yarn_player, "play")
	room.connect("item_collected", self, "_add_inventory_item")
	room.connect("item_consumed", self, "_on_Room_item_consumed")
	room.connect("paused", hud, "pause")
	room.connect("puzzle_displayed", hud, "display_puzzle")
	room.connect("set_hovered_item", hud, "set_hovered_item")
