extends "res://states/state.gd"
# Displays the collaborators in a tasteful way.


const SCROLL_SPEED = 166.6

var _scrolling := false

onready var logo = $Logo
onready var tween: Tween = $Tween


func _ready():
	logo.modulate.a = 0

	tween.interpolate_property(logo, "modulate:a", 0, 1, 2, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 1)
	tween.start()
	yield(tween, "tween_completed")

	tween.interpolate_property(logo, "modulate:a", 1, 0, 2, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 3.3475)
	tween.start()
	yield(tween, "tween_completed")

#	yield(get_tree().create_timer(8.3475), "timeout")
	_scrolling = true


func _process(delta):
	if not _scrolling:
		return

	$CreditLabels.rect_position.y -= SCROLL_SPEED * delta
	$TYFP.rect_position.y = max(540, $TYFP.rect_position.y - SCROLL_SPEED * delta)


func _on_BGM_finished():
	tween.interpolate_property($Curtain, "color:a", 0, 1, 3.33, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 3.33)
	tween.start()
	yield(tween, "tween_completed")
	emit_signal("change", "res://states/main_menu/main_menu_state.tscn")
