class_name MainMenuState
extends State
# State for main menu.


onready var _bgm: AudioStreamPlayer = $BGM
onready var _curtain: Curtain = $Curtain


func _ready():
	_curtain.fade_to(0, 1.5)

	_bgm.volume_db = linear2db(0)

	$CenterContainer/VBoxContainer/Continue.visible = Saving.has_quick()
	$CenterContainer/VBoxContainer/QuitGame.visible = OS.has_feature("pc")


func _process(_delta):
	_bgm.volume_db = linear2db(1 - _curtain.color.a)


func _on_Continue_pressed():
	_curtain.fade_to(1, 1)
	yield(_curtain, "faded")
	emit_signal("change", "res://states/gameplay/gameplay_state_starting_hallway.tscn", true)


func _on_NewGame_pressed():
	_curtain.fade_to(1, 1)
	yield(_curtain, "faded")
	emit_signal("change", "res://states/gameplay/gameplay_state_starting_cell.tscn")


func _on_QuitGame_pressed():
	_curtain.fade_to(1, 1)
	yield(_curtain, "faded")
	get_tree().quit()


func _on_ToggleFullscreen_pressed():
	emit_signal("fullscreen_toggled")
