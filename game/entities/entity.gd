class_name Entity
extends Node2D
# Base class for all in-game entities.


var is_paused := false


func _physics_process(delta: float):
	if is_paused:
		return

	_update(delta)


func _update(_delta: float):
	pass # Virtual method
