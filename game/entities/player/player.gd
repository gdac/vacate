class_name Player
extends Entity
# The player character, controlled by the user.


signal movement_completed

const SPEED_RUNNING := 666.0
const SPEED_WALKING := 444.0

export(bool) var face_left_on_ready = false

var movement_path: PoolVector2Array = []
var running := false

onready var sprite: Sprite = $SpritePivot/Sprite
onready var sprite_pivot: Position2D = $SpritePivot


func _ready():
	if face_left_on_ready:
		sprite_pivot.scale.x = -1


func _process_movement(delta: float):
	# Do nothing unless we have a movement path
	if movement_path.size() == 0:
		return

	var goal: Vector2 = movement_path[0]
	var speed: float = SPEED_RUNNING if running else SPEED_WALKING
	var distance := max(goal.distance_to(position) - speed * delta, 0.0)

	position = goal + goal.direction_to(position) * distance
	sprite.speed_scale = speed / SPEED_WALKING

	# If we have reached the goal, either move to the next point or emit signal
	if position == goal:
		movement_path.remove(0)

		if movement_path.size() == 0:
			sprite.animation = "idle"
			sprite.speed_scale = 1
			emit_signal("movement_completed")

	# Update facing direction
	if goal.x != position.x:
		sprite.animation = "walk"
		sprite_pivot.scale.x = sign(goal.x - position.x)


func _update(delta: float):
	_process_movement(delta)
