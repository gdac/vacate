class_name PuzzleBoard
extends "res://interactables/interactable.gd"
# An in-game object upon which a puzzle is set.


export(String, FILE, "*.tscn") var puzzle := ""

var completed := false


func _default_action():
	if completed:
		emit_signal("dialogue_started", "PuzzleAlreadyCompleted")
		return

	var instance: Puzzle = load(puzzle).instance()
	instance.connect("completed", self, "_on_Puzzle_completed")
	emit_signal("puzzle_displayed", instance)


func _on_Puzzle_completed():
	completed = true
	$Win.play()
	Continuity.complete_puzzle(name)
