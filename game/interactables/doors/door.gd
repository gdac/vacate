class_name Door
extends "res://interactables/interactable.gd"
# A portal which takes you from one room to another.


export(bool) var locked := false
export(String) var target_door := ""
export(String, FILE, "*.tscn") var target_scene
export(String) var unlock_item := ""

onready var sfx_close: AudioStreamPlayer2D = $Sounds/Close
onready var sfx_open: AudioStreamPlayer2D = $Sounds/Open
onready var sfx_unlock: AudioStreamPlayer2D = $Sounds/Unlock


func _process(_delta):
	if Continuity.get_progress() == 3 and locked:
		locked = false

	frame = "locked" if locked and sprite.frames.has_animation("locked") else "default"
	._process(_delta)


func _default_action():
	if locked:
		emit_signal("dialogue_started", "DoorIsLocked")
		return

	emit_signal("door_entered", target_door, target_scene)
	sfx_open.play()


func _evaluate_item(item: String):
	if locked and item != "" and item == unlock_item:
		locked = false
		emit_signal("item_consumed", unlock_item)
		emit_signal("door_unlocked")
		sfx_unlock.play()
		return false

	return ._evaluate_item(item)
