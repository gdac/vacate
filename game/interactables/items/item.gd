class_name Item
extends "res://interactables/interactable.gd"
# Item to be picked up and used by the player.


export(String, FILE, "*.png") var texture_override := ""


func _default_action():
	call_deferred("queue_free")

	var default_texture := sprite.frames.get_frame("default", 0)
	var tex = default_texture.resource_path

	if texture_override != "":
		tex = texture_override

	emit_signal("item_collected", title, tex)


func _evaluate_item(item: String):
	var combo = Inventory.get_combination(item, title)
	if combo:
		call_deferred("queue_free")
		emit_signal("item_consumed", item)
		emit_signal("item_collected", combo.result.text, combo.result.texture_path)
		return false

	return ._evaluate_item(item)
