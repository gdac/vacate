class_name Interactable
extends Area2D
# Base class for different kinds of interactable element.


signal clicked(vec2, doubleclick)
signal dialogue_started(yarn_passage)
signal door_entered(target_door, target_scene)
signal door_unlocked
signal item_collected(text, texture_path)
signal item_consumed(item)
signal puzzle_displayed(puzzle)

export(String) var title = "<GIVE OBJECT TITLE>"

var frame := "default"
var frame_suffix := ""

var _highlighted := false

onready var sprite: AnimatedSprite = $Sprite


func _process(_delta):
	if sprite:
		_update_frame()


func _unhandled_input(event: InputEvent):
	# Do nothing if releasing mouse button instead of pressing or not button
	if not (event is InputEventMouseButton) or not event.pressed or not _highlighted:
		return

	get_tree().set_input_as_handled()
	emit_signal("clicked", get_global_mouse_position(), event.doubleclick)


# Do not override.
func use(item: String = ""):
	if _evaluate_item(item):
		_default_action()


func _default_action():
	pass # Virtual method


# Performs functions based on items and returns whether or not to do default.
func _evaluate_item(item: String) -> bool:
	if item != "":
		emit_signal("dialogue_started", "ItemUseDenied")
		return false

	return true


func _on_Interactable_mouse_entered():
	_highlighted = true
	frame_suffix = "_hover"


func _on_Interactable_mouse_exited():
	_highlighted = false
	frame_suffix = ""


func _update_frame():
	if not sprite.frames:
		return

	# Try to add suffix
	if frame_suffix != "":
		var with_suffix := "%s%s" % [frame, frame_suffix]

		if sprite.frames.has_animation(with_suffix):
			sprite.animation = with_suffix
			return

		with_suffix = "default%s" % frame_suffix

		if sprite.frames.has_animation(with_suffix):
			sprite.animation = with_suffix
			return

	# Fallback to no suffix
	if sprite.frames.has_animation(frame):
		sprite.animation = frame
