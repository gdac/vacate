class_name Speaker
extends "res://interactables/interactable.gd"
# Person who can be spoken to by player - I know the name is trash lmao.


export(String) var yarn_passage := ""


func _default_action():
	emit_signal("dialogue_started", yarn_passage)


func post_dialogue():
	pass # Virtual function
