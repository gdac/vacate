class_name VentCellRoom
extends "res://interactables/speakers/speaker.gd"
# Vent inside of the cell room.


func _process(_delta):
	frame = "muffled" if Continuity.get_custom(name) else "default"


func _default_action():
	if Continuity.get_custom(name) == "muffled":
		emit_signal("dialogue_started", "%sMuffled" % yarn_passage)
		return

	._default_action()


func _evaluate_item(item: String):
	if item == "Glued Stack of Flyers" and frame == "default":
		Continuity.set_custom(name, "muffled")
		frame = "muffled"

		if Continuity.get_muffled_vent_count() >= 3:
			emit_signal("item_consumed", item)
		emit_signal("dialogue_started", "%sFlyers" % yarn_passage)
		return false

	return ._evaluate_item(item)
