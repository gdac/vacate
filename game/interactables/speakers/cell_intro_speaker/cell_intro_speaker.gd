extends "res://interactables/speakers/speaker.gd"


signal stage_requested

export(String) var item_to_give := ""
export(String, FILE, "*.png") var item_to_give_texture := ""
export(int) var stage_to_go_to := 0


func post_dialogue():
	if item_to_give != "":
		emit_signal("item_collected", item_to_give, item_to_give_texture)
		item_to_give = ""

	if stage_to_go_to != 0:
		emit_signal("stage_requested", stage_to_go_to)
		stage_to_go_to = 0
