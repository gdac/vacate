extends Node
# The root scene of the entire game.


export(String, FILE, "*.tscn") var state_path


func _init():
	# Seed the random number generator
	randomize()


func _ready():
	# Set default state
	_set_state(state_path)


func _process(_delta):
	if Input.is_action_just_pressed("force_close"):
		get_tree().quit()

	var tmp := Continuity.get_muffled_vent_count()
	for i in range(0, 3):
		AudioServer.set_bus_effect_enabled(3, i, tmp > i)


func _set_state(file_path: String, _load_game = false):
	var state: State = get_child(0)

	if state:
		state.queue_free()

	var new_state: State = load(file_path).instance()
	new_state.owner = self
	new_state.connect("change", self, "_set_state")
	new_state.connect("fullscreen_toggled", self, "_toggle_fullscreen")
	add_child(new_state)

	if _load_game:
		new_state.load_game()


func _toggle_fullscreen():
	OS.window_fullscreen = !OS.window_fullscreen
