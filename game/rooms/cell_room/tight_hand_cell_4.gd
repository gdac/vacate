extends "res://interactables/speakers/cell_intro_speaker/cell_intro_speaker.gd"


func _evaluate_item(item: String):
	if item == "Wooden Bar":
		emit_signal("item_consumed", "Wooden Bar")
		emit_signal("dialogue_started", "TightHandWithBar")
		stage_to_go_to = 5
		return false

	return ._evaluate_item(item)
