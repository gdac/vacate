extends "res://interactables/speakers/cell_intro_speaker/cell_intro_speaker.gd"


func _evaluate_item(item: String):
	if item == "Brass":
		emit_signal("item_consumed", "Brass")
		emit_signal("dialogue_started", "CrackWithBrass")
		stage_to_go_to = 7
		return false

	return ._evaluate_item(item)
