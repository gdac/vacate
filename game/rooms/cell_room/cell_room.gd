extends "res://rooms/room.gd"


onready var camera_tween: Tween = $CameraTween
onready var stages := $Stages.get_children()


func _ready():
	_set_stage(0)

	for stage in stages:
		for child in stage.get_children():
			_set_up_interactable(child)


func _on_Interactable_clicked(_vec2: Vector2, doubleclick: bool, node: Interactable):
	navigation_object = node
	node.use(inventory_bar_selection)


func _set_stage(stage: int):
	for i in stages:
		i.visible = i.get_index() == stage

	match stage:
		2:
			$Interactables/Bar.queue_free()
		3:
			$Interactables/TightHand.queue_free()
			$SFX/BreakBar.play()
		5:
			camera_tween.interpolate_property($Camera2D, "position:y",
					$Camera2D.position.y, 540, .666, Tween.TRANS_SINE)
			camera_tween.start()


func _set_up_interactable(child: Interactable):
	._set_up_interactable(child)
	child.connect("stage_requested", self, "_set_stage")


func unpause():
	.unpause()

	if navigation_object and navigation_object is Speaker:
		navigation_object.post_dialogue()
