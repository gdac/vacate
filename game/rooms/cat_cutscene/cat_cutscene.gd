extends "res://rooms/room.gd"


var _played := false


func unpause():
	if _played:
		emit_signal("change_state", "res://states/credits/credits.tscn")
		return

	_played = true
	emit_signal("dialogue_started", "EndingCat")
