extends Area2D


signal clicked(vec2, doubleclick)


func _on_Clicker_input_event(viewport, event, shape_idx):
	# Do nothing if releasing mouse button instead of pressing or not button
	if not (event is InputEventMouseButton) or not event.pressed:
		return

	emit_signal("clicked", get_global_mouse_position(), event.doubleclick)
