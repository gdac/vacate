extends "res://rooms/room.gd"


func _ready():
	if Continuity.get_progress() >= 3:
		$Interactables/HatchDoor.title = "Destroy The Machine"
		$Interactables/CatDoor.title = "Fulfil The Machine's Purpose"
