extends "res://rooms/room.gd"


var _time := 0.0

onready var attribution: VBoxContainer = $Control/Attribution
onready var jam: VBoxContainer = $Control/GameJam
onready var logo: Label = $Control/Logo
onready var tween: Tween = $Tween


func _ready():
	attribution.modulate.a = 0
	jam.modulate.a = 0
	logo.modulate.a = 0

	yield(get_tree().create_timer(2.5), "timeout")

	tween.interpolate_property(attribution, "modulate:a", 0, 1, 2.5)
	tween.start()

	yield(get_tree().create_timer(5), "timeout")

	tween.interpolate_property(attribution, "modulate:a", 1, 0, 2.5)
	tween.start()


	yield(get_tree().create_timer(5), "timeout")

	tween.interpolate_property(logo, "modulate:a", 0, 1, 2.5)
	tween.start()

	yield(get_tree().create_timer(7.5), "timeout")

	tween.interpolate_property(logo, "modulate:a", 1, 0, 2.5)
	tween.start()


	yield(get_tree().create_timer(5), "timeout")

	tween.interpolate_property(jam, "modulate:a", 0, 1, 2.5)
	tween.start()

	yield(get_tree().create_timer(5), "timeout")

	tween.interpolate_property(jam, "modulate:a", 1, 0, 2.5)
	tween.start()


func _on_AudioStreamPlayer_finished():
	emit_signal("door_entered", "", "res://rooms/cell_hallway/cell_hallway.tscn")
