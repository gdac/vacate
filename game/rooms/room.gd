class_name Room
extends Node2D
# A segment of the game map in which objects and interactivity is contained.


signal cancel_inventory_selection
signal change_state(state)
signal dialogue_started(yarn_passage)
signal door_entered(target_door, target_scene)
signal item_collected(text, texture_path)
signal item_consumed(item)
signal paused
signal puzzle_displayed(puzzle)
signal set_hovered_item(title)

const TIMES = [INF, 1000, 100, 25]

var inventory_bar_selection := ""
var is_paused := true
var navigation_object: Interactable
var shake := 0.0

onready var clicker: Area = $Clicker
onready var navigation: Navigation2D = $Navigation
onready var player: Player = $Player


func _ready():
	Continuity.set_room(name)
	var continuity = Continuity.get_continuity()

	for i in continuity.complete_puzzle:
		$Interactables.get_node(i).completed = true

	for i in continuity.remove_interactable:
		$Interactables.get_node(i).queue_free()

	for i in continuity.unlock_door:
		$Interactables.get_node(i).locked = false

	for child in $Interactables.get_children():
		_set_up_interactable(child)

	player.is_paused = true


func _physics_process(delta):
	if is_paused:
		return

	shake = max(0, shake - delta)

	var prog := Continuity.get_progress()
	var val = TIMES[prog]
	if val != INF and randi() % val == 0:
		shake = 1.0

	var cam = get_node("Player/Camera")
	if cam and cam.current:
		var dir = fmod(randf(), PI * 2.0)
		var distance = 20 - prog * 2
		cam.offset.x = cos(dir) * shake * distance
		cam.offset.y = sin(dir) * shake * distance

	if Input.is_action_just_pressed("ui_cancel"):
		pause()


func jump_player_to_door(door_name: String):
	var door: Door = get_node("Interactables").get_node(door_name)

	if not door:
		print("Cannot teleport to door '%s' as it does not exist" % door_name)
		return

	player.position = door.position
	door.sfx_close.play()


func pause(notify := true):
	is_paused = true
	player.is_paused = true
	pause_mode = PAUSE_MODE_STOP

	if notify:
		emit_signal("paused")


func unpause():
	is_paused = false
	player.is_paused = false
	pause_mode = PAUSE_MODE_INHERIT


func _navigate_to(vec2: Vector2, doubleclick: bool):
	var path := navigation.get_simple_path(player.position, vec2)

	# Remove the player's position
	path.remove(0)

	player.movement_path = path
	player.running = doubleclick


func _on_Clicker_clicked(vec2: Vector2, doubleclick: bool):
	navigation_object = null
	_navigate_to(vec2, doubleclick)
	emit_signal("cancel_inventory_selection")


func _on_Interactable_clicked(_vec2: Vector2, doubleclick: bool, node: Interactable):
	navigation_object = node
	_navigate_to(node.position, doubleclick)


func _on_Interactable_dialogue_started(yarn_passage: String):
	pause(false)
	emit_signal("dialogue_started", yarn_passage)


func _on_Interactable_door_entered(target_door: String, target_scene: String):
	pause(false)
	emit_signal("door_entered", target_door, target_scene)


func _on_Interactable_door_unlocked(child: Interactable):
	Continuity.unlock_door(child.name)
	pause(false)
	emit_signal("dialogue_started", "DoorWasUnlocked")


func _on_Interactable_item_collected(text: String, texture_path: String):
	pause(false)
	emit_signal("item_collected", text, texture_path)


func _on_Interactable_item_consumed(item: String):
	emit_signal("item_consumed", item)


func _on_Interactable_mouse_entered(child: Interactable):
	emit_signal("set_hovered_item", child.title)


func _on_Interactable_mouse_exited(_child: Interactable):
	emit_signal("set_hovered_item", "")


func _on_Interactable_puzzle_displayed(puzzle: Puzzle):
	pause(false)
	emit_signal("puzzle_displayed", puzzle)


func _on_Player_movement_completed():
	if navigation_object:
		var data = navigation_object.use(inventory_bar_selection)

		if navigation_object is Item:
			Continuity.remove_interactable(navigation_object.name)


func _set_up_interactable(child: Interactable):
	child.connect("clicked", self, "_on_Interactable_clicked", [child])
	child.connect("dialogue_started", self, "_on_Interactable_dialogue_started")
	child.connect("door_entered", self, "_on_Interactable_door_entered")
	child.connect("door_unlocked", self, "_on_Interactable_door_unlocked", [child])
	child.connect("item_collected", self, "_on_Interactable_item_collected")
	child.connect("item_consumed", self, "_on_Interactable_item_consumed")
	child.connect("mouse_entered", self, "_on_Interactable_mouse_entered", [child])
	child.connect("mouse_exited", self, "_on_Interactable_mouse_exited", [child])
	child.connect("puzzle_displayed", self, "_on_Interactable_puzzle_displayed")
